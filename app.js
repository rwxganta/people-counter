const incrementEl = document.getElementsByClassName('increment');
const saveEl = document.getElementsByClassName('save');
const previous = document.getElementById('previous');
const visualEl = document.getElementById('counter-visual');
let count = 0;

function increment() {
	count += 1
	visualEl.textContent = count;
}

function save() {
	let previousEntries = count + ' - ';
	previous.textContent += previousEntries;
	visualEl.textContent = 0;
	count = 0;
}
